#!/bin/sh

dwm_networkmanager () {
    CONNAME=$(nmcli -a | grep 'Wired connection' | awk 'NR==1{print $1}')
    if [ "$CONNAME" = "" ]; then
        CONNAME=$(nmcli -t -f active,ssid dev wifi | grep '^yes' | cut -c 5-)
    fi

    export __DWM_BAR_NETWORKMANAGER__="${SEP1}NET ${CONNAME}${SEP2}"

}

dwm_networkmanager
