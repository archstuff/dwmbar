#!/bin/sh

dwm_battery () {
    # Battery Name on E5430 is BAT0
    CHARGE=$(cat /sys/class/power_supply/BAT0/capacity)
    STATUS=$(cat /sys/class/power_supply/BAT0/status)

    printf "%s" "$SEP1"
    if [ "$STATUS" = "Charging" ]; then
        printf ">> %s%% %s" "$CHARGE" "$STATUS"
    else
        printf "== %s%% %s" "$CHARGE" "$STATUS"
    fi
    printf "%s\n" "$SEP2"
}

dwm_battery