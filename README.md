### dwmbar for Suckless DWM in Arch Linux
Modified from the dwm-bar by joestandring<br>
You can view his repo at: https://github.com/joestandring/dwm-bar<br>
I suggest using his, not mine. This is for me, and this is what I wanted.

#### Current Features:
- Battery
- Date
- Internet Connection

#### Setup:
Clone the repository in Git and enter the directory:
```bash
$ git clone https://gitlab.com/archstuff/dwmbar
$ cd dwmbar
```

Ensure you have the dependecies required for dwmbar to work, then make dwmbar.sh executable:
```bash
$ sudo pacman -Syu $(requirements.txt)
$ sudo chmod +x dwmbar.sh
```

Edit .xinitrc with the path to dwmbar:
```bash
/home/$USER/dwmbar/dwmbar.sh &
exec dwm
```
